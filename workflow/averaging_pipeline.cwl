class: Workflow
cwlVersion: v1.0
id: averaging_pipeline
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: 'Directory[]'
    'sbg:x': -88.39886474609375
    'sbg:y': 95.5
  - id: strategy
    type: File?
    'sbg:x': -467
    'sbg:y': -264
  - id: skymodel
    'sbg:x': -356.85662841796875
    'sbg:y': 130.74981689453125
outputs:
  - id: msout
    outputSource:
      - dp3_execute/msout
    type: Directory
    'sbg:x': 384.60113525390625
    'sbg:y': 155.5
steps:
  - id: aoflagger_step_generator
    in:
      - id: steps
        source:
          - preflag_step_generator/augmented_steps
      - id: strategy
        source: strategy
      - id: memoryperc
        default: 0
      - id: timewindow
        default: 0
      - id: overlapmax
        default: 0
      - id: pedantic
        default: false
      - id: keepstatistics
        default: true
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.AOFlaggerStepGenerator.cwl
    'sbg:x': -250
    'sbg:y': -102
  - id: preflag_step_generator
    in:
      - id: steps
        source:
          - channel_flagging/augmented_steps
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.PreflaggerStepGenerator.cwl
    'sbg:x': -457
    'sbg:y': -97
  - id: channel_flagging
    in:
      - id: chan
        default: '[0..nchan/32-1,31*nchan/32..nchan-1]'
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.PreflaggerStepGenerator.cwl
    'sbg:x': -620
    'sbg:y': -98
  - id: demix_step_generator
    in:
      - id: steps
        source:
          - aoflagger_step_generator/augmented_steps
      - id: baseline
        default: 'CS*,RS*&'
      - id: corrtype
        default: cross
      - id: timestep
        default: 1
      - id: freqstep
        default: 4
      - id: demixtimestep
        default: 16
      - id: demixfreqstep
        default: 64
      - id: ntimechunk
        default: 0
      - id: skymodel
        source: skymodel
      - id: instrumentmodel
        default: instrument
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.DemixerStepGenerator.cwl
    'sbg:x': -58
    'sbg:y': -99
  - id: dp3_execute
    in:
      - id: msin
        source: msin
      - id: steps
        source:
          - demix_step_generator/augmented_steps
    out:
      - id: secondary_output_files
      - id: secondary_output_directories
      - id: msout
    run: ../lofar-cwl/steps/DP3.Execute.cwl
    label: DP3.Execute
    scatter:
      - msin
    'sbg:x': 132
    'sbg:y': 61
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
