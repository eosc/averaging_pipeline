#! /bin/bash
set -e

git_clone_or_pull () {
    REPO=$1
    BRANCH=$2
    DIR=$3
    if [ -d $DIR ] 
    then
     git -C "${DIR}" pull
    else
     git clone --depth 1 -b "${BRANCH}" "${REPO}" "${DIR}"
    fi
}

# SOFTWARE VERSIONS
DP3_TAG=v4.1
IDG_TAG=0.6
LSMTool_TAG=v1.4.2
LoSoTo_TAG=f0398b
TOIL_VERSION=3.20.0
STMAN_TAG=master
Dysco_TAG=v1.2
Prefactor_TAG=master
LofarBeam_TAG=master

# FETCHES THE IDG
git_clone_or_pull https://gitlab.com/astron-idg/idg/ ${IDG_TAG} idg

# FETCHES DP3
git_clone_or_pull https://github.com/lofar-astron/DP3 ${DP3_TAG} DP3

# FETCHES STMAN
git_clone_or_pull https://github.com/lofar-astron/LofarStMan ${STMAN_TAG} STMAN

# FETCHES Dysco
git_clone_or_pull https://github.com/aroffringa/dysco.git ${Dysco_TAG} Dysco

# FETCHES LOFARBeam
git_clone_or_pull https://github.com/lofar-astron/LOFARBeam ${LofarBeam_TAG} LOFARBeam

SCRIPT_PATH=$(realpath ${BASH_SOURCE[0]})

DOCKER_PATH=$(dirname ${SCRIPT_PATH})

docker build ${DOCKER_PATH} --build-arg=IDG_TAG=${IDG_TAG}\
                            --build-arg=DP3_TAG=${DP3_TAG}\
                            --build-arg=LSMTool_TAG=${LSMTool_TAG}\
                            --build-arg=LoSoTo_TAG=${LoSoTo_TAG}\
                            --build-arg=STMAN_TAG=${STMAN_TAG}\
                            --build-arg=Dysco_TAG=${Dysco_TAG}\
                            --build-arg=TOIL_VERSION=${TOIL_VERSION}\
			    --build-arg=LofarBeam_TAG=${LofarBeam_TAG}\
                            -t lofareosc/lofar-pipeline

docker build ${DOCKER_PATH} -f ${DOCKER_PATH}/Dockerfile_ci -t lofareosc/lofar-pipeline-ci
