#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: losoto_abs

doc: |
  Normalize the solutions to a given value WEIGHT: Weights compliant

requirements:
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: |
          [norm]
          soltab = $(inputs.soltab)
          operation = NORM
          axesToNorm = $(inputs.axesToNorm)
          $(inputs.normVal !== null? 'normVal=' + inputs.normVal: '')

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: axesToNorm
    type: string[]
    doc: Axes along which compute the normalization
  - id: normVal
    type: float?
    doc: Number to normalize to vals = vals * (normVal/valsMean), by default 1.

outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)


$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
