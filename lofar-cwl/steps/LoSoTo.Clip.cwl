#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: losoto_clip

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
doc: |
  Clip solutions around the median by a factor specified by the user.
  WEIGHT: flag compliant, putting weights into median is tricky

requirements:
  InlineJavascriptRequirement:
    expressionLib:
      - { $include: utils.js}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_losoto_config('CLIP').join('\n'))

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: axesToClip
    type: string[]
    doc: axes along which to calculate the median (e.g. [time,freq])
  - id: clipLevel
    type: float?
    doc: factor above/below median at which to clip, by default 5.
  - id: log
    type: boolean?
    doc: clip is done in log10 space, by default False.
  - id: mode
    type: string?
    doc: |
      if "median" then flag at rms*clipLevel times away from the median, if it is "above",
      then flag all values above clipLevel, if it is "below" then flag all values below clipLevel.
      By default median.


outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
