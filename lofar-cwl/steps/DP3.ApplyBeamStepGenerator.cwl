#!/usr/bin/env cwl-runner

class: Workflow
cwlVersion: v1.0
id: applybeam_step_generator

inputs:
  - id: steps
    type: Any[]?
    default: []
  - id: step_name
    type: string
    doc: unique name for the step
    default: applybeam

  - id: direction
    type: string?
    doc: A RA/Dec value specifying in what direction to correct the beam. See phaseshift.phasecenter for syntax. If empty, the beam is corrected in the direction of the current phase center. 
  - id: onebeamperpatch
    type: string?
    doc: |
      Compute the beam only for the center of each patch
      (saves computation time, but you should set this to false for large
      patches). 
      In the ApplyBeam step, this setting does not make sense 
      (but it does if the applybeam is part of predict, ddecal, gaincal,
      h5parmpredict, etc.). 
      Generally, FALSE is the right setting for this option. 
      The default has changed to false in a recent (Nov 2018) version. 
  - id: usechannelfreq
    type: boolean?
    doc: |
      Compute the beam for each channel of the measurement set separately.
      This is useful for merged / concatenated measurement sets.
      For raw LOFAR data you should set it to false,
      so that the beam will be formed as in the station hardware.
      Also, setting it to false is faster.
      
  - id: updateweights
    type: boolean?
    doc: |
      Update the weights column, in a way consistent with
      the weights being inverse proportional to the autocorrelations
      (e.g. if 'autoweights' was used before).

  - id: invert
    type: boolean?
    doc: |
      Invert the corrections, to correct the data. Default is true.
      If you want to corrupt the data, set it to 'false'
     
  - id: beammode
    
    type:
      type: enum
      symbols:
       - array_factor
       - element
       - default
    doc: |
      Beam mode to apply, can be “array_factor”, “element” or “default”.
      Default is to apply both the element beam and the array factor. 
    
    
outputs:
  - id: augmented_steps
    outputSource:
      - DP3_GenericStep/augmented_steps
    type: Any[]
steps:
  - id: DP3_GenericStep
    in:
      - id: step_type
        default: 'applybeam'
      - id: step_id
        source: step_name
      - id: steps
        source: steps
      - id: parameters
        valueFrom: $(inputs)

      - id: direction
        source: direction
      - id: onebeamperpatch
        source: onebeamperpatch
      - id: usechannelfreq
        source: usechannelfreq
      - id: updateweights
        source: updateweights
      - id: invert
        source: invert
      - id: beammode
        source: beammode


#-------------------------------

    out:
      - augmented_steps
    run: ../steps/DP3.GenericStep.cwl
requirements:
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
  - class: MultipleInputFeatureRequirement
