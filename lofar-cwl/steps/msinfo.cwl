#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.1
id: msinfo

requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entryname: msinfo.py
        entry: |
          import sys
          print(sys.argv)
  - class: SchemaDefRequirement
    types:
      - $import: RadioDatatypes.yaml
hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

baseCommand:
  - python3
  - msinfo.py

inputs:
  data:
    type: RadioDatatypes.yaml#MeasurementSet
    inputBinding:
      position: 0

stdout: std.output

outputs:
  info:
    type: File
    outputBinding:
      glob: std.output
label: msinfo

