#!/usr/bin/env cwl-runner

class: Workflow
cwlVersion: v1.0
id: aoflagger_step_generator

inputs:
  - id: steps
    type: Any[]?
    default: []
  - id: step_name
    type: string
    doc: unique name for the step
    default: aoflag
  - id: count.save
    type: boolean
    default: false
    doc: >-
      If true, the flag percentages per frequency are saved to a table with
      extension .flagfreq and percentages per station to a table with extension
      .flagstat. The basename of the table is the MS name (without extension)
      followed by the stepname and extension.
  - id: count.path
    type: string
    default: ""
    doc: >-
      The directory where to create the flag percentages table. If empty, the
      path of the input MS is used.
  - id: strategy
    type: File?
    doc: >-
      The name of the strategy file to use. If no name is given, the default
      strategy is used which is fine for HBA. For LBA data the strategy
      LBAdefault should be used.
  - id: memoryperc
    type: int
    default: 0
    doc: >-
      If >0, percentage of the machines memory to use. If memorymax nor
      memoryperc is given, all memory will be used (minus 2 GB (at most 50%) for
      other purposes). Accepts only integer values (LOFAR v2.16). Limiting the
      available memory too much affects flagging accuracy; in general try to use
      at least 10 GB of memory.
  - id: memorymax
    type: float
    default: 0
    doc: >-
      Maximum amount of memory (in GB) to use. ⇐0 means no maximum. As stated
      above, this affects flagging accuracy.
  - id: timewindow
    type: int
    default: 0
    doc: >-
      Number of time slots to be flagged jointly. The larger the time window,
      the better the flagging performs. 0 means that it will be deduced from the
      memory to use. Note that the time window can be extended with an overlap
      on the left and right side to minimize possible boundary effects.
  - id: overlapperc
    type: float
    default: 0
    doc: >-
      If >0, percentage of time window to be added to the left and right side
      for overlap purposes (to minimize boundary effects). If overlapmax is not
      given, it defaults to 1%.
  - id: overlapmax
    type: int
    default: 0
    doc: Maximum overlap value (0 is no maximum).
  - id: autocorr
    type: boolean
    default: true
    doc: Flag autocorrelations?
  - id: pulsar
    type: boolean
    default: false
    doc: Use flagging strategy optimized for pulsar observations?
  - id: pedantic
    type: boolean
    default: false
    doc: Be more pedantic when flagging?
  - id: keepstatistics
    type: boolean
    default: true
    doc: Write the quality statistics?
outputs:
  - id: augmented_steps
    outputSource:
      - DP3_GenericStep/augmented_steps
    type: Any[]

steps:
  - id: DP3_GenericStep
    in:
      - id: step_type
        default: 'aoflagger'
      - id: step_id
        source: step_name
      - id: steps
        source: steps
      - id: parameters
        valueFrom: $(inputs)
      - id: count.save
        source: count.save
      - id: count.path
        source: count.save
      - id: strategy
        source: strategy
      - id: memoryperc
        source: memoryperc
      - id: memorymax
        source: memorymax
      - id: timewindow
        source: timewindow
      - id: overlapperc
        source: overlapperc
      - id: overlapmax
        source: overlapmax
      - id: autocorr
        source: autocorr
      - id: pulsar
        source: pulsar
      - id: pedantic
        source: pedantic
      - id: keepstatistics
        source: keepstatistics
    out:
      - augmented_steps
    run: ../steps/DP3.GenericStep.cwl
requirements:
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
  - class: MultipleInputFeatureRequirement
