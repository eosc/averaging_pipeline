class: CommandLineTool
cwlVersion: v1.0
id: wsclean
baseCommand:
  - wsclean
inputs:
  - id: image_size
    type: int[]
    default: [1000, 1000]
    inputBinding:
      position: 1
      prefix: -size
  - id: image_scale
    type: float
    default: 0.01
    inputBinding:
      position: 1
      prefix: -scale
      valueFrom: $(self)deg
  - id: image_name
    default: image
    type: string
    inputBinding:
      position: 1
      prefix: -name
  - id: msin
    type: Directory
    inputBinding:
      position: 2
outputs:
  dirty_image:
    type: File
    outputBinding:
      glob: $(inputs.image_name)-dirty.fits
  image:
    type: File
    outputBinding:
      glob: $(inputs.image_name)-image.fits
label: WSClean
requirements:
  - class: DockerRequirement
    dockerPull: lofareosc/lofar-pipeline-ci:latest
