class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: predict
baseCommand:
  - DPPP
inputs:
  - id: msin
    type: Directory
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
    doc: Input Measurement Set
  - default: DATA
    id: msin_datacolumn
    type: string
    inputBinding:
      position: 0
      prefix: msin.datacolumn=
      separate: false
    doc: Input data Column
  - id: storagemanager
    type: string
    default: dysco
    inputBinding
      prefix: msout.storagemanager=  
  - default: MODEL_DATA
    id: msout_datacolumn
    type: string
    inputBinding:
      position: 0
      prefix: msout.datacolumn=
      separate: false
  - id: sources_db
    type: File
    inputBinding:
      position: 0
      prefix: predict.sourcedb=
      separate: false
  - default: null
    id: sources
    type: 'string[]'
    inputBinding:
      position: 0
      prefix: predict.sources=
      separate: false
      itemSeparator: ','
      valueFrom: '[$(self)]'
  - default: false
    id: usebeammodel
    type: boolean
    inputBinding:
      position: 0
      prefix: predict.usebeammodel=True
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)
arguments:
  - 'steps=[predict]'
  - predict.beammode=array_factor
  - predict.usechannelfreq=False
  - msout=.
requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: lofareosc/lofar-pipeline-ci:latest
